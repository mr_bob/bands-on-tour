import random

bands_dict = {
    'metal': { 'bandone': ['Slayer', 'Slipknot', 'As I Lay Dying', 'In Flames', 'Iron Maiden'],
               'bandtwo' : ['Megadeth', 'Metallica', 'Pantera', 'Machinehead', 'Korn']
    },
    'punk': { 'bandone' : ['nofx', 'pennywise', 'slick shoes', 'millencolin', 'bad religion'],
              'bandtwo' : ['face to face', 'lagwagon', 'strung out', 'guttermouth', 'deviates']
    },
    'surprise me': { 'bandone': ['motley crue', 'poison', 'val halen','spinal tap', 'ratt'],
                  'bandtwo': ['creed', 'smash mouth', 'nickleback', 'fuel', 'three doors down']
    }
}

b = '\n\t>> '

def main():
    your_name = input('\nWhat is your name? ' + b)

    print('\n\tHey there ' + your_name.title() + ', lets see what bands are touring')

    print('\n\tWhat kind of music do you listen to?')

    music = True
    while music:

        user_selection = input(str(list(bands_dict.keys())) + b)

        if user_selection.lower() in bands_dict.keys():

            rahh = random.choice((bands_dict[user_selection.lower()]['bandone']))
            scream = random.choice((bands_dict[user_selection.lower()]['bandtwo']))
           
            print('\nGet your Daisy Dukes on cause ' + rahh.title() + ' and ' + scream.title() + ' are going on tour!')
            music = False

    input('Press any key to close program!')
main()